package agents;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

/**
 * This javaagent should ALWAYS be the first!
 */
public class ThreadStartTransformer implements ClassFileTransformer
{
    private static final String PROCEED = "$proceed($$); ";

    /**
     * Don't log calls to classnames if they start with the string mentioned here<br/>
     * e.g: -javaagent:thread-origin-agent-1.0.0.jar=sun/awt,sun/java2d
     */
    private final List<String> excluded = new ArrayList<>();

    public ThreadStartTransformer(final String argument)
    {
        super();

        if(argument != null)
        {
            this.excluded.addAll(Arrays.asList(argument.split(",")));
        }

    }

    /**
     * add agent
     */
    public static void premain(final String agentArgument, final Instrumentation instrumentation)
    {
        instrumentation.addTransformer(new ThreadStartTransformer(agentArgument));

        for(final Class<?> loadedClazz : instrumentation.getAllLoadedClasses())
        {
            if (loadedClazz.getName().startsWith("agents.")
                || loadedClazz.getName().startsWith("javassist")
                || loadedClazz.getName().startsWith("["))
            {
                continue;
            }

            try
            {
                instrumentation.retransformClasses(loadedClazz);
            }
            catch(final UnmodifiableClassException e)
            {
                System.out.println("Could not modify class '" + loadedClazz.getName() + "'. " + e.getMessage());
            }

        }
    }

    /**
     * instrument class
     */
    @Override
    public byte[] transform(
        final ClassLoader loader,
        final String className,
        final Class<?> clazz,
        final java.security.ProtectionDomain domain,
        final byte[] bytes)
    {
        byte[] resultingBytes = bytes;
        if(className == null)
        {
            System.out.println("ClassName was null; Class=" + clazz.getCanonicalName());
            return resultingBytes;
        }

        if(this.excluded.stream().anyMatch(className::startsWith))
        {
            System.out.println("Excluded class=" + className);
            return resultingBytes;
        }

        try
        {

            final ClassPool classPool = ClassPool.getDefault();
            final CtClass classUnderTransformation = classPool.makeClass(new java.io.ByteArrayInputStream(bytes));

            if(classUnderTransformation == null)
            {
                return resultingBytes;
            }

            classUnderTransformation.instrument(new ExprEditor()
            {

                @Override
                public void edit(final MethodCall m) {
                    CtMethod method;
                    try
                    {
                        method = m.getMethod();
                    }
                    catch(final NotFoundException e)
                    {
                        System.out.println("Could not find method '" + m.getSignature() + "':" + e.getMessage());
                        return;
                    }

                    final String classname = method.getDeclaringClass().getName();
                    final String methodName = method.getName();

                    ThreadStartTransformer.this.replaceThread(classname, m, methodName);
                }
            });

            resultingBytes = classUnderTransformation.toBytecode();

        }
        catch(final Exception e)
        {
            System.out.println(
                "Could not instrument "
                    + className
                    + "/"
                    + clazz.getCanonicalName()
                    + ", exception: "
                    + e.getMessage());
        }

        return resultingBytes;
    }

    void replaceThread(final String classname, final MethodCall m, final String methodName) {

        if(!Thread.class.getName().equals(classname))
        {
            return;
        }

        if(methodName.equals("start"))
        {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("{ ");
                sb.append("System.out.println(\"Detected Thread.start() id: [\" + ((Thread)$0).getId() + \"] name: '\" + ((Thread)$0).getName()"
                              + "+ \"'. Setting Parent Thread Id: [\" + String.valueOf(Thread.currentThread().getId()) + \"]\");");
                sb.append("System.setProperty(String.valueOf(((Thread)$0).getId()),String.valueOf(Thread.currentThread().getId()));");
                sb.append(PROCEED);
                sb.append("} ");

                m.replace(sb.toString());

            }
            catch (Throwable e) {System.out.println("Could not transform method '" + m.getMethodName() + "':" + e.getMessage());}
        }
    }

}
